//Szerző: Robotika pécs
//Program: tömb mint referencia, dinamikus memóriafoglalás

int tomb[5] = {1, 2, 3, 4, 5};
int *dinTomb;

void setup() {
  Serial.begin(9600);

  Serial.print("tomb[0] = ");
  Serial.println(tomb[0]);
  Serial.print("*tomb = ");
  Serial.println(*tomb);
  Serial.print("*(tomb + 1) = ");
  Serial.println(*(tomb + 1));
  Serial.print("tomb = ");
  Serial.println((uint32_t)tomb, HEX);
  Serial.print("&tomb = ");
  Serial.println((uint32_t)&tomb, HEX);

  for (int i = 0; i < 5; i++) {
    Serial.print("tomb[");
    Serial.print(i);
    Serial.print("] = ");
    Serial.println(*(tomb + i));
  }

  Serial.print("dinTomb = ");
  Serial.println((uint32_t)dinTomb, HEX);
  Serial.print("&dinTomb = ");
  Serial.println((uint32_t)&dinTomb, HEX);

  dinTomb = (int*)malloc(sizeof(int) * 10);
  for (int i = 0; i < 10; i++) {
    Serial.print("elotte dinTomb[");
    Serial.print(i);
    Serial.print("] = ");
    Serial.println(dinTomb[i]);
    dinTomb[i] = i;

    Serial.print("utana dinTomb[");
    Serial.print(i);
    Serial.print("] = ");
    Serial.println(dinTomb[i]);
  }

  Serial.print("dinTomb = ");
  Serial.println((uint32_t)dinTomb, HEX);
  Serial.print("&dinTomb = ");
  Serial.println((uint32_t)&dinTomb, HEX);

  dinTomb = (int*)realloc(dinTomb, sizeof(int) * 20);
  for (int i = 0; i < 20; i++) {
    if (i >= 10)
      dinTomb[i] = i;

    Serial.print("realloc dinTomb[");
    Serial.print(i);
    Serial.print("] = ");
    Serial.println(dinTomb[i]);
  }
}

void loop() {

}
