#include "MyClass.h"

MyClass::MyClass(){
  currentValue = 100;
}

MyClass::MyClass(int startValue){
  currentValue = startValue;
}

int MyClass::nextValue(){
  currentValue += this->increment;
  return currentValue;
}

int MyClass::nextValue(int increment){
  currentValue += increment;
  return currentValue;
}

void MyClass::initialize(){
  //ha szükség van olyan kódra amit csak a setupból hívhatunk meg akkor
  //azt itt kell definiálni. pl digital write
  pinMode(13, OUTPUT);
  for(int i = 0; i < 10; i++){
    digitalWrite(13, HIGH);
    delay(50);
    digitalWrite(13, LOW);
    delay(50);
  }
}
