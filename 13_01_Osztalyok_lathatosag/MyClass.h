#ifndef MyClass_h
#define MyClass_h

#include "Arduino.h"

//ez az osztály mindig más értéket tud visszaadni
//alkalmas lehet ~32000 ID kiosztására
class MyClass
{
//publikus tulajdonságok, metódusok
public:
    //konstruktor
    MyClass();
    //túlterhelt konstruktor
    MyClass(int startValue);
    //osztálymetódus
    int nextValue();
    //setup-ból hívjuk
    void initialize();
    int nextValue(int increment);
    //tulajdonságok
    int currentValue;

//privát metódusok, tulajdonságok
private:
  int increment = 5;
};

#endif
