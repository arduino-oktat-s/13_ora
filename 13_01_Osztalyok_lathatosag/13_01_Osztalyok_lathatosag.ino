//Szerző: Robotika Pécs
//Program: Osztályok

#include "MyClass.h"

//példányosítás
MyClass mc1;  //ez 100-ról indul mert az a default érték
MyClass mc2(0); //ez 0-ról indul mert azt mondtuk neki

void setup() {
  Serial.begin(9600);

  //az initialize fv-t mindig meg kell hívni
  //egyes osztályok begin-ként hivatkoznak rá. Pl. Serial.begin
  mc1.initialize();
  delay(3000);
  mc2.initialize();

  Serial.print("mc1.nextValue() = ");
  Serial.println(mc1.nextValue());
  Serial.print("mc1.nextValue() = ");
  Serial.println(mc1.nextValue());
  Serial.print("mc1.nextValue(10) = ");
  Serial.println(mc1.nextValue(10));
  Serial.print("mc1.currentValue = ");
  Serial.println(mc1.currentValue);

  
  Serial.print("mc2.nextValue(1) = ");
  Serial.println(mc2.nextValue(1));
  Serial.print("mc2.nextValue(1) = ");
  Serial.println(mc2.nextValue(1));
  Serial.print("mc2.nextValue() = ");
  Serial.println(mc2.nextValue());
  Serial.print("mc2.currentValue = ");
  Serial.println(mc2.currentValue);
}

void loop() {
}
