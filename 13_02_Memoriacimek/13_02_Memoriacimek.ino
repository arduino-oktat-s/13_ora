//Szerző: Robotika Pécs
//Program: memóriacímek, mutatók

//változó, memóriában (RAM) foglal helyet
int a = 13;

//a_p egy mutató, ami az a változó memóriacímére mutat
int *p_a = &a;

/*
   Feladat: csináljunk egy olyan függvényt,
   ami átírja a hívó paramétereket, azaz
   "kimenő" paramétereket alkalmazunk.
*/

void setup() {
  Serial.begin(9600);

  Serial.print("a = ");
  Serial.println(a);
  Serial.print("&a = ");
  Serial.println((uint32_t)&a, HEX);
  Serial.print("*p_a = ");
  Serial.println(*p_a);
  Serial.print("p_a = ");
  Serial.println((uint32_t)p_a, HEX);
  Serial.print("&p_a = ");
  Serial.println((uint32_t)&p_a, HEX);

  int K, T;
  teruletKeruletKalkulator(4, 5, &T, &K);
  Serial.print("T = ");
  Serial.println(T);
  Serial.print("K = ");
  Serial.println(K);
}

void loop() {
}

//a visszatérési értéket hibakódként használhatjuk
//nincs hiba = 0
//van hiba = -1
int teruletKeruletKalkulator(int a, int b, unsigned int *kiTerulet, unsigned int *kiKerulet) {
  if (a <= 0 || b <= 0)
    return -1;

  *kiTerulet = a * b;
  *kiKerulet = (a + b) * 2;

  return 0;
}
