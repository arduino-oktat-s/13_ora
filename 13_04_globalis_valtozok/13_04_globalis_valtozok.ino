//Szerző: Robotika Pécs
//Program: Globális változók

#include "global.h"

void setup() {
  Serial.begin(9600);

  Serial.println("GLOBAL-ok:");
  Serial.println(GLOBAL1);
  Serial.println(GLOBAL2);
  Serial.println(GLOBAL3);

  Serial.println("g-k:");
  Serial.println(g1);
  Serial.println(g2);
  Serial.println(g3);

  g1 = 4;
  g2 = 5;
  g3 = 6;

  Serial.println("g-k valtozas utan:");
  Serial.println(g1);
  Serial.println(g2);
  Serial.println(g3);

  //nem működik mert nem RAM-ban tárolódik, konstans
  //GLOBAL1 = 40;
}

void loop() {

}
